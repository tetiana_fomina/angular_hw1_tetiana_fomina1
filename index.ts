window.addEventListener('load', () => {
  resize();
  document.addEventListener('mousedown', sketch);
  document.addEventListener('mouseup', stopPainting);
  window.addEventListener('resize', resize);
});

const canvas = <HTMLCanvasElement> document.getElementById('canvas');

const ctx  = canvas.getContext('2d');
function resize() {
  ctx.canvas.width = window.innerWidth;
  ctx.canvas.height = window.innerHeight;
}

interface coord {
    x: number;
y: number;
  }

let coord: coord = { x: 0, y: 0 };
let newCoord: coord = { x: 0, y: 0 };
let paint: boolean = false;
let mySquare: number = 0;
let figureCoords:number[] = [0, 0];

function getPosition(event:any):void {
  newCoord.x = event.clientX - canvas.offsetLeft;
  newCoord.y = event.clientY - canvas.offsetTop;
}

function startPainting(event: any):void {
  paint = true;
  getPosition(event);
}
function stopPainting(): void {
  paint = false;
}

function sketch(event: any):void {
  paint = true;
  getPosition(event);
  if (!paint) return;
  ctx.beginPath();

  ctx.lineWidth = 5;

  ctx.lineCap = 'round';

  ctx.strokeStyle = 'green';

  ctx.moveTo(coord.x, coord.y);

  getPosition(event);

  ctx.lineTo(newCoord.x, newCoord.y);
  coord.x = newCoord.x;
  coord.y = newCoord.y;
  figureCoords.push(coord.x, coord.y);

  for (let i = 0; i < figureCoords.length; i++) {
    if (i % 2) {
      mySquare -= figureCoords[i] * figureCoords[i + 1] || 0;
    } else {
      mySquare += figureCoords[i] * figureCoords[i + 3] || 0;
    }
  }
  mySquare /= 2;
  mySquare = Math.abs(mySquare);
 const square =  <HTMLCanvasElement> document.querySelector('.square');
 square.textContent = `${mySquare}`;

  ctx.stroke();
}
